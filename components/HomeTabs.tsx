import React, {useEffect, useState} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Platform,
  Image,
  TextInput,
} from 'react-native';

import I18n from '../i18n/i18n';
import Settings from './Settings';
import MainScreen from './MainScreen';
import Dialog from 'react-native-dialog';
import FloatingButton from './FloatingButton';
import {globalStyles} from '../config/styles';
import ModalComponent from './ModalComponent';
import ValidatePinCode from './ValidatePinCode';
import {useUserContext} from '../controllers/user';
import {useBrandWalletContext} from '../controllers/brandWallet';

const Tab = createBottomTabNavigator();

function HomeTabs({navigation}: any) {
  const {user} = useUserContext();
  const [qrCode, setQrCode] = useState('');
  const [visible, showDialog] = useState(false);
  const {setBrandWallet} = useBrandWalletContext();
  const [erroMessage, setErroMessage] = useState('');
  const [closeReturn, setcloseReturn] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [pkCopyConfirmed, setpkCopyConfirmed] = useState(false);

  useEffect(() => {
    setpkCopyConfirmed(user?.key_copy_confirmed);
  }, [user.key_copy_confirmed]);

  const handleCancel = () => {
    showDialog(false);
  };

  const handleValidQr = () => {
    if (qrCode.startsWith('wc:')) {
      setBrandWallet((prevState: any) => {
        return {...prevState, uri: qrCode};
      });
      setTimeout(() => {
        navigation.navigate('SuccessfulConnection');
      }, 50);
      showDialog(false);
      setQrCode('');
    } else {
      setModalVisible(true);
      setErroMessage(`${I18n.t('qrScan.warningPasteQr')}`);
      showDialog(false);
      setQrCode('');
    }
  };

  useEffect(() => {
    showDialog(false);
    if (closeReturn) {
      setTimeout(() => {
        navigation.navigate('MainScreen');
      }, 50);
    }
  }, [navigation, setBrandWallet, closeReturn]);

  const changeModal = (newValue: any) => {
    showDialog(newValue);
  };

  const handleCallback = (childData: any) => {
    setcloseReturn(childData);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={[styles.tabContainer]}>
        <Tab.Navigator
          tabBarOptions={{
            showLabel: true,
            activeTintColor: '#3827B4',
            labelStyle: {fontSize: 13},
          }}>
          <Tab.Screen
            name="MainScreen"
            component={MainScreen}
            initialParams={{navigation: navigation}}
            options={{
              tabBarLabel: `${I18n.t('homeTabs.mainScreen')}`,
              tabBarIcon: ({focused}) => {
                return focused ? (
                  <Image
                    style={{width: 25, height: 25}}
                    source={require('../config/assets/inicio_active.png')}
                  />
                ) : (
                  <Image
                    style={{width: 25, height: 25}}
                    source={require('../config/assets/inicio_inactive.png')}
                  />
                );
              },
              title: 'My home',
            }}
          />
          {pkCopyConfirmed ? (
            <Tab.Screen
              name="ValidatePinCode"
              component={ValidatePinCode}
              initialParams={{navigation: navigation}}
              options={{
                tabBarLabel: `${I18n.t('homeTabs.securityScreen')}`,
                tabBarIcon: ({focused}) => {
                  return focused ? (
                    <Image
                      style={{width: 25, height: 25}}
                      source={require('../config/assets/seguridad_active.png')}
                    />
                  ) : (
                    <Image
                      style={{width: 25, height: 25}}
                      source={require('../config/assets/seguridad_inactive.png')}
                    />
                  );
                },
              }}
            />
          ) : (
            <Tab.Screen
              name="ValidatePinCode"
              component={ValidatePinCode}
              initialParams={{navigation: navigation}}
              options={{
                tabBarBadge: '!',
                tabBarLabel: `${I18n.t('homeTabs.securityScreen')}`,
                tabBarIcon: ({focused}) => {
                  return focused ? (
                    <Image
                      style={{width: 25, height: 25}}
                      source={require('../config/assets/seguridad_active.png')}
                    />
                  ) : (
                    <Image
                      style={{width: 25, height: 25}}
                      source={require('../config/assets/seguridad_inactive.png')}
                    />
                  );
                },
              }}
            />
          )}
          <Tab.Screen
            name="Settings"
            component={Settings}
            initialParams={{navigation: navigation}}
            options={{
              tabBarLabel: `${I18n.t('homeTabs.configScreen')}`,
              tabBarIcon: ({focused}) => {
                return focused ? (
                  <Image
                    style={{width: 25, height: 25}}
                    source={require('../config/assets/config_active.png')}
                  />
                ) : (
                  <Image
                    style={{width: 25, height: 25}}
                    source={require('../config/assets/config_inactive.png')}
                  />
                );
              },
            }}
          />
        </Tab.Navigator>
        <FloatingButton navigation={navigation} onChange={changeModal} />
        <View>
          <Dialog.Container visible={visible}>
            <Dialog.Title>{I18n.t('qrScan.modalPasteQr')}</Dialog.Title>
            <TextInput
              value={qrCode}
              onChangeText={setQrCode}
              style={[styles.input, globalStyles.textInputStyle]}
              placeholder="wc:a4eadb3e-e68c ..."
            />
            <Dialog.Button label={I18n.t('cancel')} onPress={handleCancel} />
            <Dialog.Button label={I18n.t('accept')} onPress={handleValidQr} />
          </Dialog.Container>
        </View>
        <ModalComponent
          erroMessage={erroMessage}
          titleModal={I18n.t('qrScan.error')}
          textButtonModal={I18n.t('close')}
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          whereFrom="Qr"
          getBack={handleCallback}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    borderTopColor: '#5120ac',
  },
  tabContainer: {
    display: 'flex',
    marginTop: 0,
    height: Platform.OS === 'ios' ? '103%' : '100%',
  },
  input: {
    fontSize: 10,
    height: 45,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#E6E6E6',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 17,
    paddingBottom: 17,
    color: '#BD446A',
    flexWrap: 'wrap',
  },
});

export default HomeTabs;
