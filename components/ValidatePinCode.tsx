import React, {useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  ImageBackground,
  Image,
  Platform,
  ActivityIndicator,
} from 'react-native';
import PINCode from '@haskkor/react-native-pincode';

import {JWT_KEY} from '@env';
import I18n from '../i18n/i18n';
import {useUserContext} from '../controllers/user';

interface Props {
  navigation: {
    navigate(destination: string, params: object): void;
    goBack: () => void;
  };
}

const jwt = require('jsonwebtoken');

function ValidatePinCode(props: any) {
  const {user, setUser} = useUserContext();
  const [loading, setLoading] = useState(false);

  const validatePin = async (pinCode: any) => {
    jwt.verify(
      user.pin,
      JWT_KEY,
      {algorithms: ['HS256']},
      function (error: any, payload: any) {
        if (error) {
          console.error(error);
        } else if (payload.pinCode === pinCode) {
          setLoading(true);
          if (props.route.params.viewRedirect) {
            setTimeout(() => {
              props.navigation.navigate(props.route.params.viewRedirect, {
                navigation: props.navigation,
              });
            }, 3000);
          } else {
            setTimeout(() => {
              props.navigation.navigate('Private', {
                navigation: props.navigation,
              });
            }, 3000);
          }
        }
      },
    );
  };

  return (
    <SafeAreaView style={styles.containerGradient}>
      <View style={styles.container}>
        <ImageBackground
          source={require('../config/assets/bgsuperior1.png')}
          resizeMode="cover"
          style={styles.sloganContainer}>
          <Image
            style={styles.logo}
            source={require('../config/assets/logo_white_beta.png')}
          />
        </ImageBackground>
      </View>
      {loading ? (
        <ActivityIndicator
          size="large"
          color={'#3827B4'}
          style={styles.loader}
        />
      ) : (
        <PINCode
          status={'enter'}
          handleResultEnterPin={validatePin}
          onClickButtonLockedPage={() => {
            if (!props.route.params.viewRedirect) {
              props.navigation.navigate('HomeTabs', {
                navigation: props.navigation,
              });
            }
          }}
          titleEnter={I18n.t('pinCode.verifyPin')}
          buttonDeleteText={I18n.t('erase')}
          titleAttemptFailed={I18n.t('pinCode.wrongPin')}
          subtitleError={I18n.t('pinCode.tryAgain')}
          maxAttempts={3}
          timeLocked={60000}
          textTitleLockedPage={I18n.t('pinCode.maxAttempts')}
          textDescriptionLockedPage={I18n.t('pinCode.blockView')}
          textSubDescriptionLockedPage={I18n.t('pinCode.comeBack')}
          textButtonLockedPage={I18n.t('close')}
          numbersButtonOverlayColor="#6C18A4"
          colorPasswordEmpty="#6C18A4"
          colorPassword="#6C18A4"
          styleLockScreenButton={styles.styleLockScreenButton}
          stylePinCodeButtonNumber="#172B4D"
          stylePinCodeColorTitle="#172B4D"
          styleLockScreenTitle={styles.styleLockScreenTitle}
          stylePinCodeButtonCircle={styles.stylePinCodeButtonCircle}
          styleAlphabet={styles.styleAlphabet}
          stylePinCodeDeleteButtonIcon="backspace"
          styleLockScreenViewIcon={styles.styleLockScreenViewIcon}
          stylePinCodeChooseContainer={styles.stylePinCodeChooseContainer}
          styleLockScreenMainContainer={styles.styleLockScreenMainContainer}
          colorPasswordError="#BD446A"
          stylePinCodeDeleteButtonColorHideUnderlay="#172B4D"
          stylePinCodeDeleteButtonColorShowUnderlay="#6C18A4"
        />
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  loader: {marginTop: 40, marginBottom: 40, flex: 1},
  styleLockScreenTitle: {color: '#172B4D'},
  styleAlphabet: {fontSize: 1},
  styleLockScreenViewIcon: {display: 'none'},
  stylePinCodeChooseContainer: {marginTop: -25},
  styleLockScreenMainContainer: {backgroundColor: '#f2f2f2'},
  styleLockScreenButton: {backgroundColor: '#6C18A4', color: 'white'},
  stylePinCodeButtonCircle: {
    backgroundColor: '#f2f2f2',
    width: 45,
    height: 45,
  },
  containerGradient: {
    justifyContent: 'center',
    height: '100%',
    marginTop: -40,
  },
  sloganContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    height: 400,
  },
  slogan: {
    color: 'white',
    fontSize: 19,
    width: 180,
    textAlign: 'center',
  },
  logo: {
    width: '50%',
    height: 100,
    resizeMode: 'contain',
    marginTop: Platform.OS === 'ios' ? 0 : 30,
  },
});

export default ValidatePinCode;
