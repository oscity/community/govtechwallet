import React from 'react';
import I18n from '../i18n/i18n';
import {Platform} from 'react-native';
import {FloatingAction} from 'react-native-floating-action';

function FloatingButton(props: any) {
  const actions = [
    {
      text: I18n.t('qrScan.scanQr'),
      icon: require('../config/assets/qrIcon.png'),
      name: 'scanQr',
      position: 1,
      color: '#6C18A4',
    },
    {
      text: I18n.t('qrScan.pasteLinkQr'),
      icon: require('../config/assets/urlIcon.png'),
      name: 'pasteLinkQr',
      position: 2,
      color: '#6C18A4',
    },
  ];

  const redirectToAction = (name: string) => {
    if (name === 'scanQr') {
      props.navigation.navigate('QrScan');
    } else {
      props.onChange(true);
    }
  };

  return (
    <FloatingAction
      actionsPaddingTopBottom={4}
      distanceToEdge={{
        vertical: Platform.OS === 'ios' ? 90 : 65,
        horizontal: Platform.OS === 'ios' ? 20 : 15,
      }}
      color="#6C18A4"
      actions={actions}
      position="right"
      onPressItem={(name?: string) => {
        if (name) {
          redirectToAction(name);
        }
      }}
    />
  );
}

export default FloatingButton;
